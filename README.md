[![Build Status](https://drone.catgirl.ai/api/badges/ext0l/sub-rosa/status.svg)](https://drone.catgirl.ai/ext0l/sub-rosa)

![A subrosian from Oracle of Seasons](./subrosian.png)

# Turn Patreon e-mails into RSS feeds

Patreon doesn't generate RSS feeds. Some people, like me, like
RSS. `sub-rosa` will scrape your Patreon e-mails, turn them into
feeds, and if you use Miniflux it'll even add them to your feed reader
for you.

See `example-config.toml` for... the example config. Put it in
`~/.config/sub-rosa/config.toml`.

## Caveats

1. Don't be a jackass with this. Store the feeds somewhere private
   behind HTTP basic auth.
2. This is very much a 'patches welcome' project; unless I want/need
   it or you supply a patch, it probably won't get done.
3. I have no clue what Patreon will think of this. That being said, my
   artist friends I asked said they're fine since as long as you're
   giving them money they don't care about clickthroughs to the posts
   themselves.
4. This needs your e-mail username and password. I suggest running
   this as a systemd timer or service on your *personal* machine,
   since anyone that compromises that is likely to own you anyway. You
   can also forward all e-mails from `bingo@patreon.com` to another
   e-mail and point sub-rosa at that, although I don't know how the
   forwarding will affect it. If you do this and it doesn't work, let
   me know and I'll fix it!
   
## The name?

It's a **sub**scription **RSS** generator.
