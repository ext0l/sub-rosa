use anyhow::{anyhow, bail, Context, Result};
use chrono::{DateTime, FixedOffset};
use log::debug;
use mailparse::{MailHeaderMap, ParsedMail};
use scraper::{ElementRef, Html, Selector};
use std::{fmt::Debug, path::PathBuf};
use url::Url;

/// A wrapper for post URLs that strips out authentication tokens in
/// the Debug formatter so they don't show up in log messages.
#[derive(Clone)]
pub struct PostUrl(pub String);

impl Debug for PostUrl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut url = Url::parse(&self.0).map_err(|_| std::fmt::Error)?;
        url.set_query(Some("auth_token_hidden"));
        url.to_string().fmt(f)
    }
}

/// What site (or whatever) a content creator posts to.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Source {
    Patreon,
}

/// Information about the content post. The `title` and `body` fields
/// are optional because SubscribeStar doesn't provide them.
#[derive(Debug)]
pub struct PostInfo {
    pub creator: Creator,
    pub post_url: PostUrl,
    pub title: Option<String>,
    pub body: Option<String>,
    pub date: DateTime<FixedOffset>,
    pub content: Option<String>,
    pub image_url: Option<String>,
    pub has_more_images: bool,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Creator {
    /// The user-friendly name.
    pub name: String,
    /// The site they post on.
    pub source: Source,
    /// Their ID, which determines the output filename.
    pub id: String,
    /// A link to their page on the subscription site.
    pub url: String,
    /// Their avatar or other icon.
    pub icon: Option<String>,
}

impl Creator {
    pub fn path(&self) -> PathBuf {
        let dir = PathBuf::from(match self.source {
            Source::Patreon => "patreon",
        });
        dir.join(&self.id).with_extension("xml")
    }
}

/// Given a message, extract information about the post. The HTML
/// given should correspond to the mail's HTML part. We need both the
/// mail *and* the HTML so we can get information such as when the
/// mail was sent.
pub fn parse_mail(mail: &ParsedMail, html: &str) -> Result<PostInfo> {
    let document = Html::parse_document(html);
    let creator = get_creator(&document).context("couldn't find creator info")?;

    let post_link = find_post_link(&document).context("couldn't find title info")?;
    let title = trimmed_text(post_link);
    let post_url = post_link
        .value()
        .attr("href")
        .ok_or_else(|| anyhow!("post link element has no href"))?
        .to_owned();

    let date = mail
        .headers
        .get_first_value("Date")
        .ok_or_else(|| anyhow!("couldn't find date header"))?;
    let date = DateTime::parse_from_rfc2822(&date).context("couldn't parse date header")?;

    let content = get_content(&document).ok();

    let image_url = get_image(&document);

    Ok(PostInfo {
        creator,
        post_url: PostUrl(post_url),
        title: Some(title),
        body: None,
        date,
        content,
        image_url,
        has_more_images: has_more_images(&document),
    })
}

fn has_more_images(document: &Html) -> bool {
    // Look for "View all N images on patreon"-type text.
    document
        .root_element()
        .text()
        .any(|text| text.starts_with("View") && text.contains("images"))
}

fn get_icon(document: &Html) -> Option<String> {
    let image_selector = Selector::parse("img").unwrap();
    let img = document.select(&image_selector).next()?;
    Some(img.value().attr("src")?.to_owned())
}

fn get_image(document: &Html) -> Option<String> {
    let image_selector = Selector::parse("a > img").unwrap();
    let img = document.select(&image_selector).next()?;
    Some(img.value().attr("src")?.to_owned())
}

// TODO: better domain check in case they change it to `patreon.com`
// (no www) or similar.

fn get_creator(document: &Html) -> Result<Creator> {
    let link_selector = Selector::parse("a[href]").unwrap();
    for link in document.select(&link_selector) {
        if trimmed_text(link).is_empty() {
            // skip over the creator's avatar, which is also a link
            continue;
        }
        let url = href_url(link)?;
        debug!("Found link to {}", url.to_string());
        // Creator links are to www.patreon.com/someid.
        if url.domain() == Some("www.patreon.com") && url.path().contains('/') {
            return Ok(Creator {
                icon: get_icon(document),
                name: trimmed_text(link),
                source: Source::Patreon,
                id: url.path().trim_start_matches('/').to_owned(),
                url: url.to_string(),
            });
        }
        // We know it's the creator id, so extract.
    }
    bail!("no links in document had a patreon creator link");
}

fn find_post_link(document: &Html) -> Result<ElementRef> {
    let link_selector = Selector::parse("a[href]").unwrap();
    for link in document.select(&link_selector) {
        let url = href_url(link)?;
        debug!("Found link to {}", url.to_string());
        if url.domain() == Some("www.patreon.com") && url.path().starts_with("/posts/") {
            return Ok(link);
        }
    }
    bail!("no post links found in document");
}

/// Gets the content of the post as *unescaped* HTML.
fn get_content(document: &Html) -> Result<String> {
    // Seems like this selector matches what we want.
    let p = Selector::parse("span > div > p").unwrap();
    let content = document
        .select(&p)
        .into_iter()
        // Crude way to strip attributes from the <p> element.
        .map(|el| format!("<p>{}</p>", el.inner_html()))
        .collect::<String>();
    if content.is_empty() {
        bail!("No content found");
    }
    Ok(content)
}

/// Find the HTML subpart of some mail. pub so we can dump the output.
pub fn find_html(mail: &ParsedMail) -> Result<String> {
    if mail.ctype.mimetype == "text/html" {
        mail.get_body().context("turning HTML body to string")
    } else {
        mail.subparts
            .iter()
            .find_map(|sub| find_html(sub).ok())
            .ok_or_else(|| anyhow!("couldn't find any HTML subparts"))
    }
}

/// Get the trimmed text content of an element.
fn trimmed_text(el: ElementRef) -> String {
    el.text().collect::<String>().trim().to_owned()
}

/// Get the URL out of an element's href. Panics if there is no href.
fn href_url(el: ElementRef) -> Result<Url> {
    let url_str = el.value().attr("href").unwrap();
    Url::parse(url_str).with_context(|| format!("couldn't parse url {}", url_str))
}
