use anyhow::{anyhow, Result};
use config::Environment;
use log::info;
use serde::Deserialize;
use std::path::{Path, PathBuf};
use url::Url;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    pub imap: Imap,
    pub miniflux: Option<Miniflux>,
    pub hosting: Hosting,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Imap {
    pub username: String,
    pub password: String,
    pub server: String,
    pub mailbox: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Miniflux {
    pub url: Url,
    pub username: String,
    pub password: String,
    pub default_category: String,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Hosting {
    /// Command to use to sync generated feeds to the server. The
    /// SUB_ROSA_OUTPUT environment variable will be set to the output
    /// directory.
    pub sync_command: String,
    pub base_url: Url,
    pub username: String,
    pub password: String,
}

impl Config {
    /// Loads the configuration file from the on-disk config path.
    ///
    /// This uses the OS-appropriate path; for example, ~/.config on Linux.
    pub fn load() -> Result<Config> {
        Config::config_path().and_then(Config::load_from)
    }

    /// Loads the configuration file from the given path.
    pub fn load_from<P: AsRef<Path>>(path: P) -> Result<Config> {
        let path = path.as_ref().to_str().ok_or_else(|| {
            anyhow!(
                "Failed to convert path '{:?}' to Unicode",
                path.as_ref().to_string_lossy()
            )
        })?;
        info!("Attempting to load config from {}", path);
        let file = config::File::new(path, config::FileFormat::Toml);
        let mut config = config::Config::new();
        config.merge(file)?;
        config.merge(Environment::with_prefix("sub_rosa_").separator("__"))?;
        let config = config.try_into()?;
        Ok(config)
    }

    /// The path to the config file.
    pub fn config_path() -> Result<PathBuf> {
        Ok(
            directories::ProjectDirs::from("ai", "deifactor", "sub-rosa")
                .ok_or_else(|| anyhow!("Failed to compute config directory path"))?
                .config_dir()
                .to_owned()
                .join("config.toml"),
        )
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn config_from_string(s: &str) -> Result<Config> {
        let mut cfg = config::Config::default();
        cfg.merge(config::File::from_str(s, config::FileFormat::Toml))?;
        Ok(cfg.try_into::<Config>()?)
    }

    #[test]
    fn nonexistent_config_path() {
        assert!(Config::load_from("/i/do/not/exist").is_err());
    }

    #[test]
    fn config_file_does_not_parse() {
        assert!(config_from_string("asldkfjaldskjf'!@#").is_err());
    }
}
