use std::collections::HashMap;

use crate::parse::{Creator, PostInfo};
use anyhow::Result;
use atom_syndication::{
    Content, Entry, EntryBuilder, Feed, FeedBuilder, Link, LinkBuilder, Person,
};
use url::Url;

pub struct FeedConverter {
    feeds: HashMap<Creator, Vec<PostInfo>>,
}

impl FeedConverter {
    pub fn new() -> Self {
        Self {
            feeds: HashMap::new(),
        }
    }

    pub fn add_post(&mut self, post: PostInfo) {
        self.feeds
            .entry(post.creator.clone())
            .or_insert_with(Vec::new)
            .push(post)
    }

    pub fn feeds(self) -> Result<HashMap<Creator, Feed>> {
        self.feeds
            .into_iter()
            .map(|(k, v)| Ok((k, Self::to_feed(&v)?)))
            .collect()
    }

    fn to_feed(posts: &[PostInfo]) -> Result<Feed> {
        assert!(
            !posts.is_empty(),
            "can't generate a feed without at least one post"
        );
        let creator = &posts[0].creator;
        let entries: Vec<_> = posts
            .iter()
            .map(Self::to_entry)
            .collect::<Result<Vec<_>>>()?;
        let updated = *entries.iter().map(|e| e.updated()).max().unwrap();
        let link = LinkBuilder::default()
            .href(creator.url.clone())
            .rel("alternate")
            .build();
        let feed = FeedBuilder::default()
            .author(Self::to_person(creator))
            .icon(creator.icon.as_ref().map(|icon| icon.replace("&", "&amp;")))
            .id(creator.url.clone())
            .links(vec![link])
            .title(creator.name.clone())
            .updated(updated)
            .entries(entries)
            .build();
        Ok(feed)
    }

    fn to_entry(post: &PostInfo) -> Result<Entry> {
        let link = Link {
            href: post.post_url.0.clone(),
            ..Link::default()
        };
        let mut entry = EntryBuilder::default();
        entry
            .title(post.title.clone().unwrap_or_else(|| "Unknown title".into()))
            .link(link)
            .id(Self::post_id(post)?)
            .updated(post.date);
        if let Some(content) = FeedConverter::generate_content(post) {
            entry.content(Some(Content {
                value: Some(content),
                content_type: Some("html".into()),
                ..Content::default()
            }));
        }
        Ok(entry.build())
    }

    fn generate_content(post: &PostInfo) -> Option<String> {
        let mut content = post.content.as_ref()?.clone();
        if let Some(image_url) = &post.image_url {
            let more_images = if post.has_more_images {
                format!(
                    "<p><a href={}>More images on Patreon</a></p>",
                    post.post_url.0
                )
            } else {
                "".into()
            };
            content = format!("<img src={}>{}{}", image_url, more_images, content);
        }
        Some(content)
    }

    fn to_person(creator: &Creator) -> Person {
        Person {
            name: creator.name.clone(),
            uri: Some(creator.url.clone()),
            email: None,
        }
    }

    /// Generates the post ID, which is a stable URI. We just replace the
    /// domain and strip query strings.
    fn post_id(post: &PostInfo) -> Result<String> {
        let mut url = Url::parse(&post.post_url.0)?;
        url.set_host("subrosa.invalid".into())?;
        url.set_query(None);
        Ok(url.into())
    }
}

#[cfg(test)]
mod test {
    use crate::parse::{PostUrl, Source};
    use chrono::{FixedOffset, TimeZone};

    use super::*;

    fn post_fixture() -> PostInfo {
        let creator = Creator {
            name: "Someone".to_owned(),
            source: Source::Patreon,
            id: "someone".to_owned(),
            url: "http://blah.invalid".to_owned(),
            icon: None,
        };
        PostInfo {
            creator,
            post_url: PostUrl("http://what.invalid".to_owned()),
            title: Some("Some content".to_owned()),
            body: Some("The body".to_owned()),
            // when i was writing this code
            date: FixedOffset::west(8).ymd(2021, 08, 23).and_hms(0, 46, 38),
            content: Some("stuff goes here".to_owned()),
            image_url: None,
            has_more_images: false,
        }
    }

    #[test]
    fn entry_sets_all_required_fields() {
        let entry = FeedConverter::to_entry(&post_fixture()).unwrap();
        assert_ne!(entry.id(), "", "id not set");
        assert_ne!(entry.title(), "", "title not set");
        assert_ne!(
            entry.updated(),
            Entry::default().updated(),
            "updated not set"
        );
    }

    #[test]
    fn feed_sets_all_required_fields() {
        let feed = FeedConverter::to_feed(&[post_fixture()]).unwrap();
        assert!(!feed.authors.is_empty(), "no author");
        assert_ne!(feed.id(), "", "id not set");
        assert_ne!(feed.title(), "", "title not set");
        assert_ne!(feed.updated(), Feed::default().updated(), "updated not set");
    }

    #[test]
    fn feed_escapes_xml() {
        let mut post = post_fixture();
        post.content = Some("<i>Tim &amp; Eric</i>".to_string());
        let feed = FeedConverter::to_feed(&[post]).unwrap();
        let s = feed.to_string();
        println!("{}", s);
        assert!(s.contains("&lt;i&gt;Tim &amp;amp; Eric&lt;/i&gt;"));
    }
}
