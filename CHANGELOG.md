# CHANGELOG

## 1.1.0 - 2022-03-27

* [`57c0a754`][57c0a754]: Add support for looking in a specific folder (thanks to AstraLuma)
* [`57435f84`][57435f84]: Proper entity escaping of HTML in feed content

[57c0a754]: https://codeberg.org/ext0l/sub-rosa/commit/57c0a754ff8eb88d43fcd18d2c365b2d45146b6c
[57435f84]: https://codeberg.org/ext0l/sub-rosa/commit/57435f84c4c8bc570ae4e5891946af66198ecc96

## 1.0.0 - 2021-10-09

Initial release.
